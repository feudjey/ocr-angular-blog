import { Injectable } from '@angular/core';
import { Post } from '../models/post.model';
import { Subject } from 'rxjs';

import * as Firebase from 'firebase/app';
import 'firebase/database';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  
  public subject: Subject<Post[]> = new Subject();
  private posts: Post[] = [];

  public constructor() {
  }

  public getColorName(post: Post): string {
    let colorName: string;

    if (post.likeCount - post.dislikeCount > 0) {
      colorName = 'green';
    } else if (post.likeCount - post.dislikeCount < 0) {
      colorName = 'red';
    }

    return colorName;
  }

  public addLike(post: Post): void {
    post.likeCount ++;

    this.saveAll();
  }

  public addDislike(post: Post): void {
    post.dislikeCount ++;

    this.saveAll();
  }

  public create(title: string, content: string): void {
    this.posts.push(new Post(title, content));

    this.saveAll();
  }

  public update(id: number, title: string, content: string): void {
    this.posts[id].title = title;
    this.posts[id].content = content;
    this.posts[id].updatedAt = Date.now();

    this.saveAll();
  }

  public delete(id: number): void {
    this.posts.splice(id, 1);

    this.saveAll();
  }

  public findOne(id: number): PromiseLike<Post> {
    return new Promise(
        (resolve, reject) => {
          Firebase.database().ref('/posts/' + id).once('value').then(
              (data) => {
                resolve(data.val());
              }, (error) => {
                reject(error);
              });
        });
  }

  public fetchAll(): void {
    Firebase.database().ref('/posts').on(
        'value',
        (data) => {
          this.posts = data.val() ? data.val() : [];

          this.emitSubject();
        });
  }

  private saveAll(): void {
    this.sortByLikeBalance();
    Firebase.database().ref('/posts').set(this.posts);

    this.emitSubject();
  }

  private sortByLikeBalance(): void {
    this.posts.sort(
        function(a: Post, b: Post): number {
          let order: number;
      
          if (a.likeCount - a.dislikeCount < b.likeCount - b.dislikeCount) {
            order = 1;
          } else if (a.likeCount - a.dislikeCount > b.likeCount - b.dislikeCount) {
            order = -1;
          } else {
            order = 0;
          }

          return order;
        });   
  }

  private emitSubject(): void {
    this.subject.next(this.posts);
  }
}

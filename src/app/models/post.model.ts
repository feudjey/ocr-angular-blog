export class Post {

  public createdAt: number = Date.now();
  public updatedAt: number;
  public likeCount: number = 0;
  public dislikeCount: number = 0;

  public constructor(
      public title: string,
      public content: string) {
  }
}
import { Component } from '@angular/core';
import * as Firebase from 'firebase/app';
import 'firebase/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public constructor() {
    var firebaseConfig = {
      apiKey: "AIzaSyCc2xUGM8iE1utNtQ1Aor4CaouxYJx-C2c",
      authDomain: "blog-337e3.firebaseapp.com",
      databaseURL: "https://blog-337e3.firebaseio.com",
      projectId: "blog-337e3",
      storageBucket: "",
      messagingSenderId: "844529738014",
      appId: "1:844529738014:web:15b8592270c7fbaeb0690f"
    };

    Firebase.initializeApp(firebaseConfig);
  }
}

import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PostService } from 'src/app/services/post.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {

  private group: FormGroup;
  private id: number;

  public constructor(
      private service: PostService,
      private builder: FormBuilder,
      private router: Router,
      private activatedRoute: ActivatedRoute) {
  }

  public ngOnInit(): void {
    this.group = this.builder.group(
        {
          title: ['', [Validators.required]],
          content: ['', [Validators.required]]
        });
    
    if (this.activatedRoute.snapshot.params['id']) {
      this.id = this.activatedRoute.snapshot.params['id'];
      this.service.findOne(this.id).then(
          (post: Post) => {
            this.group.controls['title'].setValue(post.title);
            this.group.controls['content'].setValue(post.content);
          });
    } else {
      this.id = -1;
    }
  }

  private onSave(): void {
    const title: string = this.group.get('title').value;
    const content: string = this.group.get('content').value;

    if (this.id === -1) {
      this.service.create(title, content);
    } else {
      this.service.update(this.id, title, content);
    }

    this.router.navigate(['/posts']);
  }

  private onCancel(): void {
    if (confirm('Are you sure ?')){
      this.router.navigate(['/posts']);
    }
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { PostService } from 'src/app/services/post.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-index',
  templateUrl: './post-index.component.html',
  styleUrls: ['./post-index.component.scss']
})
export class PostIndexComponent implements OnInit, OnDestroy {

  private posts: Post[];
  private subscription: Subscription;

  public constructor(
      private service: PostService,
      private router: Router) {
  }

  public ngOnInit(): void {
    this.subscription = this.service.subject.subscribe(
        (posts: Post[]) => {
          this.posts = posts;
        });

    this.service.fetchAll();
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}

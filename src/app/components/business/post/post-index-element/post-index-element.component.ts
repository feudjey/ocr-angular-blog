import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { PostService } from 'src/app/services/post.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-post-index-element',
  templateUrl: './post-index-element.component.html',
  styleUrls: ['./post-index-element.component.scss']
})
export class PostIndexElementComponent implements OnInit {
  
  @Input()
  private post: Post;

  @Input()
  private id: number;

  public constructor(
      private service: PostService,
      private router: Router) {
  }

  public ngOnInit(): void {
  }

  private onLike(): void {
    this.service.addLike(this.post);
  }

  private onDislike(): void {
    this.service.addDislike(this.post);
  }

  private onEdit(id: number): void {
    this.router.navigate(['/posts', id]);
  }

  private onDelete(id: number): void {
    if (confirm('Are you sure ?')){
      this.service.delete(id);
    }
  }

  private getColor(): string {
    return this.service.getColorName(this.post);
  }
}

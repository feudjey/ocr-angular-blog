import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostIndexComponent } from './components/business/post/post-index/post-index.component';
import { PostIndexElementComponent } from './components/business/post/post-index-element/post-index-element.component';
import { PostFormComponent } from './components/business/post/post-form/post-form.component';
import { HeaderComponent } from './components/common/header/header.component';
import { BodyComponent } from './components/common/body/body.component';
import { PostService } from './services/post.service';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'posts', component: PostIndexComponent },
  { path: 'posts/new', component: PostFormComponent },
  { path: 'posts/:id', component: PostFormComponent },
  { path: '', redirectTo: 'posts', pathMatch: 'full' },
  { path: '**', redirectTo: 'posts' }
];

@NgModule({
  declarations: [
    AppComponent,
    PostIndexComponent,
    PostIndexElementComponent,
    PostFormComponent,
    HeaderComponent,
    BodyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    PostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
